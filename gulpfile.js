var gulp = require('gulp'),
  pug = require('gulp-pug'),
  del = require('del');

gulp.task('clean', () => {
  return del(['./src/**/*.html','./www']);
})
gulp.task('pugwatch', (done) => {
  gulp.src('./src/**/*.pug')
    .pipe(pug({
      pretty: true,
      doctype: 'html'
    }))
    .pipe(gulp.dest('./src/'))
    .on('end', done)
})
gulp.task('watch', () => {
  gulp.watch(['./src/**/*.pug'], ['pugwatch']).on('change', (e) => {
    console.log('Resource file ' + e.path + ' has been changed.');
  })

})
gulp.task('pug', ['clean'], (done) => {
  gulp.src('./src/**/*.pug')
    .pipe(pug({
      pretty: true,
      doctype: 'html'
    }))
    .pipe(gulp.dest('./src/'))
    .on('end', done)
})

gulp.task('build', ['pug']);
