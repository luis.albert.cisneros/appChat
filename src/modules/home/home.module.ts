import { NgModule } from "@angular/core";
import { IonicModule } from "ionic-angular";
import { HomePage } from "./pages/home/home";
import { ChatModule } from "../chat/chat.module";
import { UserModule } from "../user/user.module";

@NgModule({
    imports:[
        IonicModule,
        ChatModule,
        UserModule
    ],
    declarations:[
        HomePage
    ],
    entryComponents:[
        HomePage
    ],
    providers:[

    ],
    exports:[
        IonicModule
    ]
})
export class HomeModule{}