import { NgModule } from "@angular/core";
import { IonicModule } from "ionic-angular";

import { HeadersService } from "./providers/headers.service";
import { ConfigService } from "./providers/config.service";
import { CommonService } from "./providers/common.service";

@NgModule({
    imports:[
        IonicModule
    ],
    declarations:[

    ],
    entryComponents:[

    ],
    providers:[
 
        HeadersService,
        ConfigService,
        CommonService
    ],
    exports:[

    ]
})
export class CoreModule {}