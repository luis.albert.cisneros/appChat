import { Observable } from "rxjs/Observable";

export interface IReadService<T> {

    query: (criteria?: any) => Observable<T[]>;
    getById: (_id: string) => Observable<T>;
}
export interface IWriteService<T> {

    save: (item: any) => Observable<T>;
    remove: (_id: string) => Observable<T>;
}