import { Injectable } from "@angular/core";

const ENV = 'local';
const IP = '192.168.42.82';
const PORT = 3000;
@Injectable()
export class ConfigService{

    baseUrl: string;
    constructor(

    ){
        this.baseUrl = this._getBaseUrl();
    }

    private _getBaseUrl():string{
        let urls = {
            local: `http://${IP}:${PORT}/`,
            dev: 'https://midominio.com/',
            production: 'https://midominio.com/'
        }
        return urls[ENV];
    }
}