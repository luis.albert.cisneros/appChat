import { Injectable } from "@angular/core";
import { ConfigService } from "./config.service";

@Injectable()
export class CommonService {

    constructor(
        private _configSrv: ConfigService
    ) {

    }

    buildApiUrl(endPoint: string, ...params: string[]): string {
        // of "192.168.42.82:3000" to "192.168.42.82:3000/"
		if (!endPoint.endsWith('/')) endPoint += '/';
		//example ENV: 192.168.42.82:3000/ + api/users
		let apiUrl = this._configSrv.baseUrl+ endPoint;
		// of ['param1','param2',...]  to "param1/param2/..."
		let extendUrl = params.join('/')
		//example:    192.168.42.82:3000/api/users/detail
		return apiUrl + extendUrl;
    }

    normalizeQuery(endPoint:string, criteria:any):string{
        let apiUrl = this.buildApiUrl(endPoint).slice(0,-1);
        return apiUrl + this._buildQueryString(criteria);
    }

    private _buildQueryString(criteria:any):string{
        let query = '?';
        Object.keys(criteria).forEach(key=>{
            if(criteria[key]) query += `${key}=${criteria[key]}&`;
        })
        return query.slice(0,-1);
    }
}