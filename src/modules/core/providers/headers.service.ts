import { Injectable } from "@angular/core";
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class HeadersService{

    build(params:any){
        return new HttpHeaders()
            .set('Content-Type','application/json');


    }
}