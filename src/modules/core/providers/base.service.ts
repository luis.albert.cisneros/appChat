import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { HeadersService } from "./headers.service";
import { CommonService } from "./common.service";
import { Observable } from "rxjs/Observable";
import { IReadService, IWriteService } from "./base.interfaces";

const METHODS = {
    GET: 'get',
    POST: 'post',
    PUT: 'put',
    DELETE: 'delete'
}

export class BaseService<T> implements IReadService<T>, IWriteService<T>{

    constructor(
        protected _endpoint: string,
        protected _http: HttpClient,
        protected _headersSrv: HeadersService,
        protected _commonSrv: CommonService
    ) {

    }

    query(criteria: any): Observable<T[]> {
        let url = this._commonSrv.normalizeQuery(this._endpoint, criteria);
        return this._request(url, METHODS.GET);
    }
    save(item: any): Observable<T> {
        if (item._id) return this._update(item);
        else return this._create(item);
    }
    getById(id: string): Observable<T> {
        let url = this._commonSrv.buildApiUrl(this._endpoint, id);
        return this._request(url, METHODS.GET);
    }
    remove(id: string): Observable<T> {
        let url = this._commonSrv.buildApiUrl(this._endpoint, id);
        return this._request(url, METHODS.DELETE);
    }
    private _create(item: any): Observable<any> {
        let url = this._commonSrv.buildApiUrl(this._endpoint);
        return this._request(url, METHODS.POST, item);
    }
    private _update(item: any): Observable<any> {
        let url = this._commonSrv.buildApiUrl(this._endpoint, item._id);
        return this._request(url, METHODS.PUT, item);
    }
    private _request(url: string, method = METHODS.GET, item: any = {}): Observable<any> {
        let options = { headers: this._getHeaders() };

        let _http = this._http[method](url, options);

        if (method == METHODS.POST || method == METHODS.PUT) {
            _http = this._http[method](url, item, options);
        }

        return _http.map(res => {
            let data = res.json();
            if (data.success) return data.result;
            else throw data.error;
        });
    }
    private _getHeaders() {
        return this._headersSrv.build({});
    }

}