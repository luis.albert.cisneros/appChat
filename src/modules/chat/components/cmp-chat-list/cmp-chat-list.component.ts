import { Component } from "@angular/core";
import { ChatService } from "../../providers/data/chat.service";
import { IChat } from "../../models/chat.interface";
import { NavController } from "ionic-angular";
import { ChatDetailPage } from "../../pages/chat-detail/chat-detail";

@Component({
    selector: 'cmp-chat-list',
    templateUrl: 'cmp-chat-list.component.html'
})
export class CmpChatListComponent {
    chats: IChat[] = [];
    constructor(
        private _chatSrv: ChatService,
        private _navCtrl: NavController
    ) {
        this._getChats();
    }
    goToDetail(chat:any){
        this._navCtrl.push(ChatDetailPage,{ chatId: chat._id });
    }
    private _getChats() {
        this._chatSrv.getChats()
            .subscribe(
                chats => { 
                    this.chats = chats; 
                },
                error => { 
                    console.log(error); 
                }
            )
    }
}