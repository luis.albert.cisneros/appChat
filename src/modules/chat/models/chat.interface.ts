export interface IChat {

    _id?: string;
    createdBy?: any;
    messages?: any[];
    participants?: any[];
}