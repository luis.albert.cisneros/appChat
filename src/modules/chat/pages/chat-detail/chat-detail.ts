import { Component } from "@angular/core";
import { ChatService } from "../../providers/data/chat.service";
import { IChat } from "../../models/chat.interface";
import { NavParams } from "ionic-angular";

@Component({
    selector: 'chat-detail-page',
    templateUrl: 'chat-detail.html'
})
export class ChatDetailPage {
    chat: IChat = {};
    currentMessage: string = '';
    user: any;
    title: string = '';
    messages: any[] = [];
    constructor(
        private _chatSrv: ChatService,
        private _navParams: NavParams
    ) {
        let chatId = this._navParams.get('chatId');
        this.user = this._navParams.get('user');
        if (chatId) this._loadChat(chatId);
        if (this.user) this._findChat();
    }

    send() {
        if (this.currentMessage) {
            let message = { text: this.currentMessage, sender: { _id: 'u009', firstName: 'L. Albert', lastName: 'Cisneros', profile: '../assets/imgs/profile.jpg' }, chatId: this.chat._id }
            if(this.chat && this.chat._id){
                this.chat.messages.push(message);
                this._buildMessages();
            }else{
                this._createChat(this.user)
                    .then(chat=>{
                        this.chat = chat;
                        this.chat.messages.push(message);
                        this._buildMessages();
                    })
            }
        }
    }
    private _findChat() {
        this._chatSrv.getChatExists('u009', this.user._id)
            .subscribe(
                chat => {
                    if (chat) {
                        this.chat = chat;
                        this.title = chat.name;
                        this._buildMessages();
                    } else {
                        this.title = this.user.firstName + ' ' + this.user.lastName;
                    }
                },
                error => {
                    console.log(error);
                }
            )
    }
    private _loadChat(chatId) {
        this._chatSrv.getChatById(chatId)
            .subscribe(
                chat => {
                    this.chat = chat;
                    this.title = chat.name;
                    this._buildMessages();
                    console.log(this.chat);
                },
                error => {
                    console.log(error);
                }
            )
    }
    private _buildMessages() {
        this.chat.messages.forEach(msg => {
            msg['isSender'] = (msg.sender._id == 'u009');
        });
    }

    private _createChat(user):Promise<any>{

        return new Promise((resolve, reject)=>{
            let chat = {
                _id:'c002',
                createdBy:{ _id: 'u009', firstName:'L. Albert', lastName: 'Cisneros', profile:'../assets/imgs/profile.jpg'},
                participants:[ user ],
                messages:[]
            };
            return Promise.resolve(chat);
        })
        
    }
}