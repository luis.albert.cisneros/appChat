import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { DB } from "../../../common/database-test";
import { BaseService } from "../../../core/providers/base.service";
import { HttpClient } from "@angular/common/http";
import { HeadersService } from "../../../core/providers/headers.service";
import { CommonService } from "../../../core/providers/common.service";


const END_POINT: string = 'api/chats'
@Injectable()
export class ChatService extends BaseService<any> {

    constructor(
        public _http: HttpClient,
        public _headersSrv: HeadersService,
        public _commonSrv: CommonService
    ) {
        super(END_POINT, _http, _headersSrv, _commonSrv);
    }

    getChats(): Observable<any[]> {
        return new Observable(observer => {
            observer.next(DB.chats);
            observer.complete();
        })
    }
    getChatById(chatId: string): Observable<any> {
        return new Observable(observer => {
            let _chat: any = {};
            DB.chats.forEach(chat => {
                if (chat._id == chatId) _chat = chat;
            })
            let chatUpdated = this._asignNameToChat(_chat);
            observer.next(chatUpdated);
            observer.complete();
        });
    }
    getChatExists(sessionUserId, memberId): Observable<any> {
        return new Observable(observer => {
            let chatFinder: any;
            DB.chats.forEach(chat => {
                let createdId = chat.createdBy._id;
                let participantId = chat.participants[0]._id;
                if ((createdId == sessionUserId && participantId == memberId) || (createdId == memberId && participantId == sessionUserId)) {
                    chatFinder = this._asignNameToChat(chat);
                }
            })
            observer.next(chatFinder);
            observer.complete();
        })
    }
    private _asignNameToChat(chat) {
        let userId = 'u009';
        if (chat.createdBy._id == userId) {
            chat.name = chat.participants[0].firstName + ' ' + chat.participants[0].lastName;
        } else {
            chat.name = chat.createdBy.firstName + ' ' + chat.createdBy.lastName;
        }
        return chat;
    }

}