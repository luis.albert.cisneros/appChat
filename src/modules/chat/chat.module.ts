import { NgModule } from "@angular/core";
import { IonicModule } from "ionic-angular";
import { CmpChatListComponent } from "./components/cmp-chat-list/cmp-chat-list.component";
import { ChatService } from "./providers/data/chat.service";
import { ChatDetailPage } from "./pages/chat-detail/chat-detail";

@NgModule({
    imports:[
        IonicModule
    ],
    declarations:[
        CmpChatListComponent,
        ChatDetailPage
    ],
    entryComponents:[
        CmpChatListComponent,
        ChatDetailPage
    ],
    providers:[
        ChatService
    ],
    exports:[
        CmpChatListComponent,
        IonicModule
    ]
})
export class ChatModule{}