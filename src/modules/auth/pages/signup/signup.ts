import { Component } from "@angular/core";
import { Nav } from "ionic-angular";
import { HomePage } from "../../../home/pages/home/home";
import { LoginPage } from "../login/login";

@Component({
    selector: 'signup-page',
    templateUrl: 'signup.html'
})
export class SignupPage {

    constructor(
        private _nav: Nav
    ) { }

    login() {
        this._nav.setRoot(LoginPage);
    }
    signup() {
        this._nav.setRoot(HomePage);
    }
}