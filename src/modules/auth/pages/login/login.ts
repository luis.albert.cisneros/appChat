import { Component } from "@angular/core";
import { NavController, Nav } from "ionic-angular";
import { HomePage } from "../../../home/pages/home/home";
import { SignupPage } from "../signup/signup";

@Component({
    selector:'login-page',
    templateUrl:'login.html'
})
export class LoginPage{

    constructor(
        private _navCtrl: NavController,
        private _nav: Nav
    ){

    }
    login(){
       this._nav.setRoot(HomePage);
    }
    signup(){
        this._navCtrl.push(SignupPage);
    }
}