import { NgModule } from "@angular/core";
import { IonicModule } from "ionic-angular";
import { LoginPage } from "./pages/login/login";
import { SignupPage } from "./pages/signup/signup";

@NgModule({
    imports:[
        IonicModule,
        
    ],
    declarations:[
        LoginPage,
        SignupPage
    ],
    entryComponents:[
        LoginPage,
        SignupPage
    ],
    providers:[

    ],
    exports:[

    ]
})
export class AuthModule {}