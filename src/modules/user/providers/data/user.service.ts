import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { DB } from "../../../common/database-test";
import { BaseService } from "../../../core/providers/base.service";
import { HttpClient } from "@angular/common/http";
import { HeadersService } from "../../../core/providers/headers.service";
import { CommonService } from "../../../core/providers/common.service";

const END_POINT = '/api/users';
@Injectable()
export class UserService extends BaseService<any>{

    constructor(
        public _http: HttpClient,
        public _headersSrv: HeadersService,
        public _commonSrv: CommonService
    ) {
        super(END_POINT, _http, _headersSrv, _commonSrv)
    }

    getContacts(): Observable<any> {
        return new Observable(observer => {
            let contacts: any[] = [], excludeId = 'u009';
            DB.Users.forEach(user => {
                if (user._id != excludeId) contacts.push(user);
            });
            observer.next(contacts);
            observer.complete();

        })
    }
    getUserById(userId: string): Observable<any> {
        return new Observable(observer => {
            let _user: any;
            DB.Users.forEach(user => {
                if (user._id == userId) _user = user;
            })
            observer.next(_user);
            observer.complete();
        })
    }

}