import { Component } from "@angular/core";
import { Nav } from "ionic-angular";
import { HomePage } from "../../../home/pages/home/home";

@Component({
    selector: 'favorites-page',
    templateUrl: 'favorites.html'
})
export class FavoritesPage{

    constructor(
        private _nav: Nav
    ){
        
    }
    back(){
        this._nav.setRoot(HomePage);
    }
}