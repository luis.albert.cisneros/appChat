import { Component } from "@angular/core";
import { Nav } from "ionic-angular";
import { HomePage } from "../../../home/pages/home/home";
import { UserService } from "../../providers/data/user.service";

@Component({
    selector: 'my-profile-page',
    templateUrl: 'my-profile.html'
})
export class MyProfilePage {
    user: any = {};
    constructor(
        private _nav: Nav,
        private _userSrv: UserService
    ) {
        this._getUser();
    }

    back() {
        this._nav.setRoot(HomePage);
    }

    private _getUser() {
        this._userSrv.getUserById('u009')
            .subscribe(
                user => {
                    this.user = user;
                },
                error => {
                    console.log(error);
                }
            )
    }
}
