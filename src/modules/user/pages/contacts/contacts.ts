import { Component, ViewChild } from "@angular/core";
import { Nav } from "ionic-angular";
import { HomePage } from "../../../home/pages/home/home";

@Component({
    selector: 'contacts-page',
    templateUrl:'contacts.html'
})
export class ContactsPage{
    
    // @ViewChild(Nav) nav: Nav
    constructor(
       private _nav: Nav
    ){
        
    }
    back(){
        this._nav.setRoot(HomePage);
    }
}