import { Component } from "@angular/core";
import { UserService } from "../../providers/data/user.service";
import { NavController } from "ionic-angular";
import { ChatDetailPage } from "../../../chat/pages/chat-detail/chat-detail";

@Component({
    selector: 'cmp-users-list',
    templateUrl: 'cmp-users-list.component.html'
})
export class CmpUsersListComponent {

    users: any[] = [];
    constructor(
        private _userSrv: UserService,
        private _navCtrl: NavController
    ) {
        this._loadUsers();
    }
    goToChat(user){
        this._navCtrl.push(ChatDetailPage,{ user });
    }
    private _loadUsers(){
        this._userSrv.getContacts()
            .subscribe(
                users=>{
                    this.users = users;
                    console.log(this.users);
                },
                error=>{
                    console.log(error);
                }
            )
    }
}