import { NgModule } from "@angular/core";
import { IonicModule } from "ionic-angular";
import { MyProfilePage } from "./pages/my-profile/my-profile";
import { ContactsPage } from "./pages/contacts/contacts";
import { FavoritesPage } from "./pages/favorites/favorites";
import { UserService } from "./providers/data/user.service";
import { CmpUsersListComponent } from "./components/cmp-users-list/cmp-users-list.component";

@NgModule({
    imports:[
        IonicModule
    ],
    declarations:[
        MyProfilePage,
        ContactsPage,
        FavoritesPage,
        CmpUsersListComponent
    ],
    entryComponents:[
        MyProfilePage,
        ContactsPage,
        FavoritesPage,
        CmpUsersListComponent
    ],
    providers:[
        UserService
    ],
    exports:[
        CmpUsersListComponent,
        IonicModule
    ]
})
export class UserModule {}