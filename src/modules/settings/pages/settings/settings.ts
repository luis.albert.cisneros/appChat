import { Component } from "@angular/core";
import { Nav } from "ionic-angular";
import { HomePage } from "../../../home/pages/home/home";

@Component({
    selector:'settings-page',
    templateUrl:'settings.html'
})
export class SettingsPage{
    
    constructor(
        private _nav:Nav
    ){
        
    }
    back(){
        this._nav.setRoot(HomePage);
    }
}