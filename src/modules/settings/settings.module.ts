import { NgModule } from "@angular/core";
import { IonicModule } from "ionic-angular";
import { SettingsPage } from "./pages/settings/settings";

@NgModule({
    imports:[
        IonicModule
    ],
    declarations:[
        SettingsPage
    ],
    entryComponents:[
        SettingsPage
    ],
    providers:[

    ],
    exports:[

    ]
})
export class SettingsModule {}