export class DB {

    static get Users() {
        return [
            { _id: 'u001', firstName: 'Patrick', lastName: 'Keenan', profession: 'Gerente de Gestion humana', username: 'patrick@mail.com', profile: 'assets/imgs/110/guy-1.jpg' },
            { _id: 'u002', firstName: 'Mary', lastName: 'Jonhson', profession: 'Product Manager', username: 'mary@mail.com', profile: 'assets/imgs/110/guy-2.jpg' },
            { _id: 'u003', firstName: 'Peter', lastName: 'Carlsson', profession: 'Tester Analyst', username: 'peter@mail.com', profile: 'assets/imgs/110/guy-3.jpg' },
            { _id: 'u004', firstName: 'Trevor', lastName: 'Hansen', profession: '.Net Engineer 3', username: 'trevor@mail.com', profile: 'assets/imgs/110/guy-4.jpg' },
            { _id: 'u005', firstName: 'Amar', lastName: 'Sagoo', profession: 'Business Owner', username: 'amar@mail.com', profile: 'assets/imgs/110/woman-2.jpg' },
            { _id: 'u006', firstName: 'Abbey', lastName: 'Christensen', profession: 'IT Delivery Consultant LATAM', username: 'abbey@mail.com', profile: 'assets/imgs/110/woman-3.jpg' },
            { _id: 'u007', firstName: 'Ali', lastName: 'Connors', profession: 'Gerente de reclutamiento y seleccion', username: 'ali@mail.com', profile: 'assets/imgs/110/woman-4.jpg' },
            { _id: 'u008', firstName: 'Alex', lastName: 'Nelson', profession: 'Gerente de atraccion y gestion de talento', username: 'alex@mail.com', profile: 'assets/imgs/110/guy-5.jpg' },
            {
                _id: 'u009',
                firstName: 'L. albert',
                lastName: 'Cisneros',
                profession: 'Full Stack Javascript',
                username: 'patrick@mail.com',
                profile: 'assets/imgs/profile.jpg',
                age: 24,
                gender: 'Male',
                bio: " I'm a sytems  engineer and like music :)",
                socialLink: {
                    web: 'devcis.com'
                },
                address: {
                    street: 'Urb Sebastian Barranca N-11',
                    city: 'Ica',
                    country: 'Peru',
                    Zip: '',
                    State: ''
                },
                phone: {
                    cell: '985755925',
                    office: '',
                    fax: ''
                }
            }
        ]
    }
    static get chats() {
        return [
            {
                _id: 'c001',
                createdBy: {_id: 'u001', firstName: 'Patrick', lastName: 'Keenan', profession: 'Gerente de Gestion humana', username: 'patrick@mail.com', profile: 'assets/imgs/110/guy-1.jpg'},
                createdOn: '06/06/2018',
                messages: [{
                    _id: 'm001', text:'Hola amigo',attachments:[],chatId: 'c001',createdOn:'06/06/2018',sender:{_id: 'u001', firstName: 'Patrick', lastName: 'Keenan', profession: 'Gerente de Gestion humana', username: 'patrick@mail.com', profile: 'assets/imgs/110/guy-1.jpg'}
                }],
                participants:[
                    
                    {  _id: 'u009',firstName: 'L. albert',lastName: 'Cisneros',profession: 'Full Stack Javascript',username: 'patrick@mail.com',profile: 'assets/imgs/profile.jpg',
                        age: 24,gender: 'Male', bio: " I'm a sytems  engineer and like music :)",socialLink: { web: 'devcis.com' },
                        address: { street: 'Urb Sebastian Barranca N-11', city: 'Ica', country: 'Peru',Zip: '', State: ''},
                        phone: { cell: '985755925', office: '', fax: '' }
                    }
                ]
            }
        ]
    }

}