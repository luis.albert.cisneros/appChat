import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthModule } from '../modules/auth/auth.module';
import { HomeModule } from '../modules/home/home.module';
import { UserModule } from '../modules/user/user.module';
import { ChatModule } from '../modules/chat/chat.module';
import { SettingsModule } from '../modules/settings/settings.module';
import { CoreModule } from '../modules/core/core.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    AuthModule,
    HomeModule,
    UserModule,
    ChatModule,
    SettingsModule,
    CoreModule
  ],
  declarations: [
    MyApp,

    ListPage
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,

    ListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
