import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { ListPage } from '../pages/list/list';
import { HomePage } from '../modules/home/pages/home/home';
import { MyProfilePage } from '../modules/user/pages/my-profile/my-profile';
import { ContactsPage } from '../modules/user/pages/contacts/contacts';
import { SettingsPage } from '../modules/settings/pages/settings/settings';
import { FavoritesPage } from '../modules/user/pages/favorites/favorites';
import { LoginPage } from '../modules/auth/pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{ title: string, icon: string, component: any }>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private _alertCtrl: AlertController,
    private _menuCtrl: MenuController
  ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'My Profile', icon: 'person', component: MyProfilePage },
      { title: 'Contacts', icon: 'people', component: ContactsPage },
      { title: 'Favorites', icon: 'star', component: FavoritesPage },
      { title: 'Configuration', icon: 'settings', component: SettingsPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  exit(){
    let alert = this._alertCtrl.create({
      title:'Sign out!',
      subTitle:'close session?',
      buttons:[
        {
          text:'Cancel',
          role:'cancel'
        },
        {
          text:'Accept',
          handler:()=>{
            this._menuCtrl.close();
            this.nav.setRoot(LoginPage);
          }
        }
      ]
    })
    alert.present();
  }
  removeAccount(){
    let alert = this._alertCtrl.create({
      title:'Delete Account!',
      subTitle:'Are you sure to delete your account?',
      buttons:[
        {
          text:'Cancel',
          role:'cancel'
        },
        {
          text:'Confirm',
          handler:()=>{
            this._menuCtrl.close();
            this.nav.setRoot(LoginPage);
          }
        }
      ]
    })
    alert.present();
  }
}
